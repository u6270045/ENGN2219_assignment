function [ Zeta ] = zeta(Delta)
Phi=15*(T-12);
Psi = 37.44;
Zeta = acosd (sind(Psi)*sind(Delta) + cosd(Psi)*cosd(Delta)*cosd(Phi));
end
