addpath('Users/yudong/Desktop/engn2219');
A= importdata('grid-config.txt');
B= importdata('circle-config.txt');
d=10; % day of year when you want to do the calculation
w=1;  %counter
Psi = 37.44;
for T=1:1:24
    for i=1:1:400
        l= A(i,1);
        h=A(i,2);
        L= sqrt((l*l + h*h ));
        phi= hour_angle(T);
        delta= Delta(d);
       
        Zeta = acosd (sind(Psi)*sind(delta) + cosd(Psi)*cosd(delta)...
            *cosd(phi));
        alpha = 90 - Zeta;
        beta= Beta(L,alpha);
        Soalpha = Solar(alpha);
        
        Insolation = Soalpha * 6 .* cos(beta);
        Inso_1(i)=abs(Insolation);
    end
    
    for j=1:1:400
        l= A(j,1);
        h=A(j,2);
        L= sqrt((l*l + h*h ));
        phi= hour_angle(T);
        delta= Delta(d);
       
        Zeta = acosd (sind(Psi)*sind(delta) + cosd(Psi)*cosd(delta)...
            *cosd(phi));
        alpha = 90 - Zeta;
        beta= Beta(L,alpha);
        Soalpha = Solar(alpha);
        
        Insolation = Soalpha .* 6 .* cosd(beta);
        Inso_2(j)=abs(Insolation);
    end
    

   Isum_1 = sum(Inso_1);
   Isum_2 = sum(Inso_2);
   
   daydata_1(w)= Isum_1;
   daydata_2(w)= Isum_2;
   w=w+1;
   
end
plot(T,daydata_1(w));
plot(T,daydata_2(w));
Output_1 = 365*sum(daydata_1(w))*180/3600/10^6;
Output_2 = 365*sum(daydata_2(w))*180/3600/10^6;