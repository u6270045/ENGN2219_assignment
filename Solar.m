function [Solar_alpha] = Soolar(alpha)
k = 0.21;
S0 = 1000;
if alpha > 0
    Solar_alpha = S0 * exp(-k/sind(alpha));
elseif alpha == 0
    Solar_alpha = 0;
elseif alpha < 0
    Solar_alpha = S0 * exp(-k/sind(alpha));    
else
    disp('error')
end
end 
