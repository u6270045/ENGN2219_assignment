function [ Delta ] = solardeclination( d )
%UNTITLED2 This is a function to calculate solar declination angle
%  d is the day of the year
Delta = -23.5 * cosd((360/365)*(d+10))

end

